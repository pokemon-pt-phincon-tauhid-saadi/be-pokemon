"use strict";

const Repository = use("Repository");

class PokemonRepository extends Repository {
    static async probabilityScrap(percent) {
        return Math.random() < percent;
    }

    static async isPrimerNumber(num) {
        if (num <= 1) return false; // negatives
        if (num % 2 == 0 && num > 2) return false; // even numbers
        const s = Math.sqrt(num); // store the square to loop faster
        for (let i = 3; i <= s; i += 2) {
            // start from 3, stop at the square, increment in twos
            if (num % i === 0) return false; // modulo shows a divisor was found
        }
        return true;
    }

    static async fibonacci(num, pokemon) {
        if(pokemon.includes('-')) {
          pokemon = pokemon.trim().substring(0,  pokemon.trim().indexOf('-'))
        }
        if (num == 1) return pokemon + '-' + 0;
        if (num == 2) return pokemon + '-' + 1;
        var num1 = 0;
        var num2 = 1;
        var sum;
        var i = 2;
        while (i < num) {
            sum = num1 + num2;
            num1 = num2;
            num2 = sum;
            i += 1;
        }
        return pokemon + '-' + num2;
    }
}

module.exports = PokemonRepository;
