'use strict';

const isGroup = true;
const groupName = 'pokemon';
const groupMiddleware = null;
const routes = [
    {
        url: '/check/probability',
        verbs: ['GET'],
        action: 'probability',
        middleware: null
    },
    {
      url: '/check/primernumber/:idNumber',
      verbs: ['GET'],
      action: 'primernumber',
      middleware: null
    },
    {
      url: '/do/rename',
      verbs: ['POST'],
      action: 'doRename',
      middleware: null
    }
];

module.exports = { isGroup, groupName, groupMiddleware, routes };
