'use strict';

const Controller = use('Controller');

const PokemonRepo = use('App/Domains/Pokemon/Repositories/PokemonRepository');

class PokemonController extends Controller {

    probability({request, response}) {
      PokemonRepo.probabilityScrap(0.5)
        .then((resp) => {
            this.dataResponse(response, 200, resp);
        })
        .catch((error) => {
            this.errorResponse(response, 500, error.stack);
        });
    }

    primernumber({request, response}) {
      const { idNumber } = request.params;
      PokemonRepo.isPrimerNumber(idNumber)
        .then((resp) => {
            this.dataResponse(response, 200, resp);
        })
        .catch((error) => {
            this.errorResponse(response, 500, error.stack);
        });
    }

    doRename({request, response}) {
      const { id, name, renamed } = request.all();
      PokemonRepo.fibonacci(renamed, name)
        .then((resp) => {
            this.dataResponse(response, 200, resp);
        })
        .catch((error) => {
            this.errorResponse(response, 500, error.stack);
        });
    }

    update({request, response}) {
        //
    }

    destroy({request, response}) {
        //
    }

}

module.exports = PokemonController;
